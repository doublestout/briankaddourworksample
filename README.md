




## How to Clone a repository
Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files..


## Run Project in Visual Studio
1. This project was created using visual studio 2017 community which is freely available here .[Visual Studio 2017](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15)
2. Open Project SLN in visual studio choose Solution Platform drop down
3. Select x64 or x86, if you are unsure use x86. This is required from CEFSharp

## Run EXE
1. You should be able to click and run the EXE, but make sure you keep the HTML file with the EXE file

## Thie Project Uses Bucket Manager from Autodesk as an example
1. That project is available here .[Bucket Manager from Auto Desk](https://github.com/Autodesk-Forge/bucket.manager-csharp-sample.tool)

---